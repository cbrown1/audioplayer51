# -*- coding: UTF-8 -*-

class GetKeyCode:
  
  def __init__(self):
    import ConfigParser
    import Tkinter as tk
    
    # Initialise the Window
    self.root = tk.Tk()
    self.root.title('GetKeycode')
    self.root.winfo_toplevel().resizable(tk.NO, tk.NO)
    self.disp = tk.Label(self.root)
    self.disp.pack()
    self.disp.config(width=15,font=("Helvetica", 13, 'bold'), text="hit a key!")

    self.root.bind("<KeyPress>", self.keypress)

    self.root.mainloop()
  
  def keypress(self, event):
    self.disp.config(text=str(event.keycode))
  
ThisApp = GetKeyCode()
