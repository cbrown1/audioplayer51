#!/usr/bin/env python

# This script retrieves metadata about a cd from cddb, allows you to edit, 
# writes a CUE sheet, rips the cd audio using cdparanoia, encodes to FLAC, 
# then embeds the metadata as FLAC tags.

import DiscID, CDDB, sys, os, shutil
from sys import argv as argv
  
cdrom = None

#if len(sys.argv) >= 1:
#  cddevice = sys.argv[0]
#else:
cddevice = "/dev/cdrom"

if cddevice:
  cdrom = DiscID.open(cddevice)
else:
  cdrom = DiscID.open()
disc_id = DiscID.disc_id(cdrom)
(query_stat, query_info) = CDDB.query(disc_id)
if query_stat != 200 and query_stat != 210 and query_stat != 211:
  print "failure getting disc info, status %i\n" % query_stat
else:
  print query_info["disc_id"]
