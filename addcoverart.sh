#!/bin/sh

if [ $# -lt 2 ] ; then
	echo "Usage: $0 <flacfile> <imagefile>"
	exit
fi

FLAC="$1"
IMAGE="$2"

TAGFILE=`mktemp`

printf "COVERART=" > $TAGFILE
base64 -w0 "$IMAGE" >> $TAGFILE
echo >> $TAGFILE

metaflac --import-tags-from=$TAGFILE "$FLAC"
rm $TAGFILE

