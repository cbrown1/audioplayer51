;NSIS Modern User Interface
;Basic Example Script
;Written by Joost Verburg

;--------------------------------
;Include Modern UI

  !include "MUI.nsh"

;--------------------------------
;General

  ;Name and file
  Name "AudioPlayer51"
  OutFile "AudioPlayer51_win32.exe"

  ;Default installation folder
  InstallDir "$PROGRAMFILES\AudioPlayer51"
  
;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "license.txt"
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "Install"

  SetOutPath "$INSTDIR"
  
  File /r audioplayer51\dist\*.*

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

  CreateDirectory "$SMPROGRAMS\AudioPlayer51"
  CreateShortCut "$SMPROGRAMS\AudioPlayer51\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\AudioPlayer51\AudioPlayer51.lnk" "$INSTDIR\AudioPlayer51.exe" "" "$INSTDIR\AudioPlayer51.ico" 0
  CreateShortCut "$SMPROGRAMS\AudioPlayer51\License.lnk" "$INSTDIR\License.txt"
  CreateShortCut "$SMPROGRAMS\AudioPlayer51\Edit Preferences.lnk" "$INSTDIR\AP51Config.ini"

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\AudioPlayer51" "DisplayName" "AudioPlayer51"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\AudioPlayer51" "UninstallString" "$INSTDIR\uninstall.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\AudioPlayer51" "DisplayIcon" "$INSTDIR\AudioPlayer51.ico,0"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\AudioPlayer51" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\AudioPlayer51" "NoRepair" 1

SectionEnd

;--------------------------------
;Descriptions

  ;Assign language strings to sections
;  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
;  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  RMDir /r "$INSTDIR"
  RMDir /r "$SMPROGRAMS\AudioPlayer51"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\AudioPlayer51"

SectionEnd
