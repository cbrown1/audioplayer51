' vbscript to download source package, compile, build installer, and upload to server

dim fso, objShell, objProc, delfolder, srcdir
srcdir = "audioplayer51"
Set fso=CreateObject("Scripting.FileSystemObject")
Set objShell = WScript.CreateObject("WScript.Shell")

' UNTESTED:
Set fle=fso.CreateTextFile(objShell.CurrentDirectory & "\ftpscript.txt", True)
fle.WriteLine "open ftp.audioplayer51.org"
fle.WriteLine "ap51"
fle.WriteLine "lockn54"
fle.WriteLine "binary"
fle.WriteLine "cd /public_html/downloads"
fle.WriteLine "get " & srcdir & "_src.zip"
fle.WriteLine "close"
fle.WriteLine "quit"
fle.Close
objShell.Run "ftp ""-s:" & objShell.CurrentDirectory & "\ftpscript.txt""", 1, True
'Set ex=objShell.Exec("ftp ""-s:" & objShell.CurrentDirectory & "\ftpscript.txt""")
'Do While ex.Status = 0
'    WScript.Sleep 100
'Loop
fso.DeleteFile objShell.CurrentDirectory & "\ftpscript.txt"

If fso.FolderExists(objShell.CurrentDirectory & "\" & srcdir) Then
  set delfolder = fso.GetFolder(objShell.CurrentDirectory & "\" & srcdir)
  delfolder.Delete 
End If

objShell.Run "7z x -y """ & objShell.CurrentDirectory & "\" & srcdir & "_src.zip""", 1, True
'Set ex=objShell.Exec("7z x -y """ & objShell.CurrentDirectory & "\" & srcdir & "_src.zip""")
'Do While ex.Status = 0
'    WScript.Sleep 100
'Loop


Set fle=fso.CreateTextFile(objShell.CurrentDirectory & "\" & srcdir & "\buildexe.py", True)
fle.WriteLine "from distutils.core import setup"
fle.WriteLine "import py2exe"
fle.WriteLine ""
fle.WriteLine "setup("
fle.WriteLine "  windows=["
fle.WriteLine "    {"
fle.WriteLine "      ""script"": ""AudioPlayer51.py"","
fle.WriteLine "      ""icon_resources"": [(1, ""AudioPlayer51.ico"")]"
fle.WriteLine "    }"
fle.WriteLine "  ],"
fle.WriteLine "  data_files=["
fle.WriteLine "    ("
fle.WriteLine "    """",[""AP51Config.ini"",""AudioPlayer51.ico"",],"
fle.WriteLine "    )"
fle.WriteLine "  ]"
fle.WriteLine ")"
'fle.Close

objShell.CurrentDirectory = objShell.CurrentDirectory & "\" & srcdir

objShell.Run "C:\Python25\python.exe """ & objShell.CurrentDirectory & "\buildexe.py"" py2exe", 1, True

If not fso.FolderExists(objShell.CurrentDirectory & "\dist\images") Then
  fso.CreateFolder(objShell.CurrentDirectory & "\dist\images")
End If
fso.CopyFolder objShell.CurrentDirectory & "\images", objShell.CurrentDirectory & "\dist\images", True

objShell.CurrentDirectory = "..\"
'
fso.CopyFile objShell.CurrentDirectory & "\AP51Config.ini", objShell.CurrentDirectory & "\" & srcdir & "\dist\AP51Config.ini"
fso.CopyFile objShell.CurrentDirectory & "\license.txt", objShell.CurrentDirectory & "\" & srcdir & "\dist\license.txt"
objShell.Run "upx --best """ & srcdir & "\dist\AudioPlayer51.exe""", 1, True
objShell.Run "upx --best """ & srcdir & "\dist\audiere.dll""", 1, True
objShell.Run "upx --best """ & srcdir & "\dist\python25.dll""", 1, True
objShell.Run "upx --best """ & srcdir & "\dist\tk84.dll""", 1, True
objShell.Run "upx --best """ & srcdir & "\dist\tcl84.dll""", 1, True
'
objShell.Run """C:\Program Files\NSIS\makensis.exe"" /V1 AudioPlayer51.nsi", 1, True

set delfolder = fso.GetFolder(objShell.CurrentDirectory & "\" & srcdir & "\dist")
delfolder.Delete 
set delfolder = fso.GetFolder(objShell.CurrentDirectory & "\" & srcdir & "\build")
delfolder.Delete 
set delfolder = fso.GetFolder(objShell.CurrentDirectory & "\" & srcdir)
'delfolder.Delete True
'fso.DeleteFile objShell.CurrentDirectory & "\" & srcdir & "\buildexe.py"

Set fle=fso.CreateTextFile(objShell.CurrentDirectory & "\ftpscript.txt", True)
fle.WriteLine "open ftp.audioplayer51.org"
fle.WriteLine "ap51"
fle.WriteLine "lockn54"
fle.WriteLine "binary"
fle.WriteLine "cd /public_html/downloads"
fle.WriteLine "delete AudioPlayer51_win32.exe"
fle.WriteLine "put AudioPlayer51_win32.exe"
fle.WriteLine "close"
fle.WriteLine "quit"
fle.Close
objShell.Run "ftp ""-s:" & objShell.CurrentDirectory & "\ftpscript.txt""", 1, True

'Set ex=objShell.Exec("ftp ""-s:" & objShell.CurrentDirectory & "\ftpscript.txt""")
'Do While ex.Status = 0
'   WScript.Sleep 100
'Loop
fso.DeleteFile objShell.CurrentDirectory & "\ftpscript.txt"
fso.DeleteFile objShell.CurrentDirectory & "\" & srcdir & "_src.zip"
