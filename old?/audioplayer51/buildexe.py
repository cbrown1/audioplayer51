from distutils.core import setup
import py2exe

setup(
  windows=[
    {
      "script": "AudioPlayer51.py",
      "icon_resources": [(1, "AudioPlayer51.ico")]
    }
  ],
  data_files=[
    (
    "",["AP51Config.ini","AudioPlayer51.ico",],
    )
  ]
)
