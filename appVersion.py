class versionInfo:
  """A simple class to handle some of the basic functions involved with app versions. 
  
  checkVer() is a simple function to allow the user to check if he has the most 
  recent version. When called, it downloads a text file at the location specified in 
  'versionLink' (the file should contain the most recent version of the app, and 
  nothing else), and compare it to the current version. If the two differ (simple
  string comparison, since it is assumed the the retrieved version is always the 
  most recent) a message box is displayed to notify the user.
  
  showAbout() displays an about box, with version info and a clickable weblink.
  """

  name = 'AudioPlayer51'
  version = '0.2'
  versionLink = "http://www.audioplayer51.org/ap51ver.txt"
  gotoLink = "http://audioplayer51.org"
  copyright = "(C) 2007-2009 Christopher Brown"
  author = "Christopher Brown"
  contact = "cbrown@code-breaker.com"
  description = "A lo-fi interface for hi-fi audio enthusiasts who use FLAC files and CUE sheets"
  shortdescription = "A FLAC/CUE player"

  def __init__(self):
    pass

  def checkVer(self):
    """Retrieves version info from specified URL, and compares that version with current.
    
      A simple function to allow the user to check if he has the most recent version.
      When called, it downloads a text file at the location specified in 'versionLink'
      (the file should contain the most recent version of the app, and nothing else), 
      and compare it to the current version. If the two differ (simple string 
      comparison, since it is assumed the the retrieved version is always the most 
      recent) a message box is displayed to notify the user.
      """
    from urllib2 import urlopen
    import tkMessageBox
    try:
      thatversion = urlopen(self.versionLink).read().strip()
      if thatversion != self.version:
        sdisplay = "A newer version of %s is available!\n\nYour version: %s\nNewest version: %s\n\nGet it at %s!" % (self.name, self.version, thatversion, self.gotoLink)
        tkMessageBox.showinfo(self.name,sdisplay)
    except IOError:
      try:
        test = urlopen('http://www.google.com').read().strip()
        print "Error checking version: Version information not found"
      except IOError:
        print "Error checking version: No internet connection"
        
  def showAbout(self, parent, image, audiolib):
    """showAbout() displays an about box, with version info and a clickable weblink."""

    import Tkinter as tk
    top = self.top = tk.Toplevel(parent)
    top.winfo_toplevel().resizable(tk.NO, tk.NO)
    top.deiconify()
    top.transient(parent)
    top.grab_set()
    top.config(background="white")
    
    from sys import version as pyver
    from ImageTk import PhotoImage
    spyver = pyver.split(" ")[0]
    stkver = str(tk.TkVersion)
    
    self.aboutimage = PhotoImage(file=image)
    tk.Label(top, image=self.aboutimage).pack()
    tk.Label(top, background='#444477', foreground='#FFFFFF', width=25, text=self.name + ' v' + self.version, font=("Helvetica", 13, 'bold'), pady=4).pack()
    tk.Label(top, background="#FFFFFF", foreground='#000000', width=35, font=("Helvetica", 9, 'normal'), text="Audio Library: " + audiolib).pack()
    tk.Label(top, background="#FFFFFF", foreground='#000000', width=25, font=("Helvetica", 9, 'normal'), text="Python v" + spyver + " TkInter v" + stkver).pack()
    tk.Label(top, background="#FFFFFF", foreground='#000000',  font=("Helvetica", 9, 'normal'), text=self.description, wraplength=250, pady=10).pack()
    tk.Label(top, background="#FFFFFF", foreground='#000000',  font=("Helvetica", 9, 'normal'), text='Copyright ' + self.copyright + "\n" + self.contact).pack()
    www = tk.Label(top, background="#FFFFFF", foreground="#7ab025", width=25, font=("Helvetica", 10, 'normal'), text=self.gotoLink, cursor="hand1")
    www.bind('<Button-1>', self._loadwww)
    www.pack()
    b = tk.Button(top, text="Alright!", command=self._ok)
    b.pack(pady=5)

  def _loadwww(self, url):
    import webbrowser
    webbrowser.open(self.gotoLink)

  def _ok(self):
    self.top.destroy()

